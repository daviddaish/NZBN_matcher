# Data prep jupyter

Dockerfile taken largely from this [jupyter/Data Science Stack Dockerfile](https://github.com/jupyter/docker-stacks/blob/master/datascience-notebook/Dockerfile),
which is [documented here](https://github.com/jupyter/docker-stacks/tree/master/datascience-notebook).

## Google Cloud Container engine work

### Sending the image to google cloud container registry

The GCloud container registry is a private, project specific docker image
registry great for hosting images before they are deployed to a kubernetes
cluster.

To send this image to the GCloud container registry, first build the image
locally. Then tag the local image appropriately for GCloud. Then push the image
to the registry using the Cloud SDK. The steps to do so are detailed below.

```
docker build -t data-prep-jup ./data_prep_jupyter
docker tag data-prep-jup asia.gcr.io/nzbn-matching-service/data-prep-jup
gcloud docker -- push asia.gcr.io/nzbn-matching-service/data-prep-jup
```

### Updating the image on kubernetes

As there is only one jupyter app running at any one time, rolling out a new
update will kill the kurnel. This command list also assumes you've already
updated the image on the GCloud container registry.

```
kubectl set image deploy/data-prep-jup-deployment data-prep-jup=asia.gcr.io/nzbn-matching-service/data-prep-jup:latest --record
```
or
```
kubectl set image deploy/data-prep-jup-deployment data-prep-jup=asia.gcr.io/nzbn-matching-service/data-prep-jup --record
```

## Local work

### Building the image locally

To build the image from the root directory of this project:

```
sudo docker build -t data-prep-jup .
```

### Running the image locally

To run the image with the docker shell tool, use this command, substituting
`<password>` with your password of choice:

```
sudo docker run -it --rm -p 8888:8888 data-prep-jup start-notebook.sh --NotebookApp.token='<password>'
```

The app can then be accessed at `localhost:8888/?token=<password>`
