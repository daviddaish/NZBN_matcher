# The NZBN matcher

This is the source code for a number docker images intended to be deployed to
google cloud container engine. Together, they are intended to act as a blackbox
api where databases go in, then come out with the rows mapped to [NZBN numbers](https://www.companiesoffice.govt.nz/companies/learn-about/nzbn).

This application is inteded to assist with the introduction of the NZBN number
to legacy NZ government databases. In addition, it's to add to my portfolio.

## The microservices

### Data prep jupyter
This image is a jupyter app for preprocessing, manipulating and exploring the
data used to train the various machine learning applications that make up
various parts of this application.

## Setting up the development environment
For Ubuntu:
```
cd ~/
sudo apt-get install docker git
sudo usermod -a -G docker ${USER}
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get install google-cloud-sdk kubectl
gcloud init
```
Then restart your computer


## Guides
A series of guides and notes that were used to build this documentation and
codebase in no particular order:

* [Data science jupyter notebook repo](https://github.com/jupyter/docker-stacks/tree/master/datascience-notebook)
* [Good jupyter saving on a docker image](https://stackoverflow.com/questions/36756907/tensorflow-on-docker-how-to-save-the-work-on-jupyter-notebook)
* [Google Cloud container regisitry documentation](https://cloud.google.com/container-registry/docs/quickstart)
* [EZ Docker without sudo](https://cloud.google.com/container-registry/docs/quickstart)
* [Great guide to kubernetes rolling updates](https://tachingchen.com/blog/Kubernetes-Rolling-Update-with-Deployment/)
